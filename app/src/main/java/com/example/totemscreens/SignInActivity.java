package com.example.totemscreens;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by user on 1/18/2016.
 */
public class SignInActivity extends Activity {
    private ImageView mImageView1;
    private TextView mTextView;
    private Button mButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_gmailsignin);

        mImageView1 = (ImageView) findViewById(R.id.slide_previous);
        mImageView1.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
                Intent in = new Intent(SignInActivity.this, RecommendedPlanActivity.class);
                startActivity(in);
                finish();
            }
        });

        mButton = (Button) findViewById(R.id.push_button);
        mButton.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
                Intent i = new Intent(SignInActivity.this, MobileActivity.class);
                startActivity(i);
                finish();
            }
        });

        mTextView = (TextView) findViewById(R.id.textView2);

        String msg1 = "By signing in you agree to ";
        String msg2 = "<font color='#000000'>terms.</font>";
        String msg3 = " More on ";
        String msg4 = "<font color='#000000'>Privacy.</font>";

        mTextView.setText(Html.fromHtml(msg1 + msg2 + msg3 + msg4));

    }
}
