package com.example.totemscreens;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by user on 1/20/2016.
 */
public class OTPAlternateActivity extends Activity {

    private Button mSubmitButton;
    private EditText mEditText;
    private TextView mTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_otpalternate);

        mEditText = (EditText) findViewById(R.id.edit_number);
        mEditText.setSelection(mEditText.getText().length());

        mSubmitButton = (Button) findViewById(R.id.submit_button);
        mSubmitButton.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
               if (validate()) {
                   Intent in = new Intent(OTPAlternateActivity.this, ProfileActivity.class);
                   startActivity(in);
                   finish();
               } else{
                   onOtpFailed();
                   return;
               }
            }
        });

        mTextView = (TextView) findViewById(R.id.textView2);

        String msg1 = "Din't recieve OTP? ";
        String msg2 = "<font color='#1a8cff'>Request New + </font>";

        mTextView.setText(Html.fromHtml(msg1 + msg2));

        mTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "New OTP Requested !!",
                        Toast.LENGTH_SHORT).show();
            }
        });
    }

    public boolean validate() {
        boolean valid = true;
        String otpNumber = mEditText.getText().toString();

        if (otpNumber.isEmpty() || otpNumber.length() < 6 || otpNumber.length() > 6) {
            mEditText.setError("Incorrect OTP Number");
        } else {
            mEditText.setError(null);
        }
        return valid;
    }

    public void onOtpFailed(){
        Toast.makeText(getBaseContext(), "Incorrect OTP", Toast.LENGTH_LONG).show();

        mSubmitButton.setEnabled(true);
    }
}
