package com.example.totemscreens;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Gallery;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by user on 1/20/2016.
 */
public class ProfileActivity extends Activity {

    private TextView mTextView;
    private ImageButton mImageButton;
    private Button mSaveButton;
    private EditText mEditText;
    private SharedPreference sharedPreference;
    Activity context = this;
    private String region;
    Gallery gallery;
    ImageAdapter galleryImageAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_profile);
        sharedPreference = new SharedPreference();
        mImageButton = (ImageButton) findViewById(R.id.back_topbar);

        mImageButton.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
                Intent in = new Intent(ProfileActivity.this, OTPAlternateActivity.class);
                startActivity(in);
                finish();
            }
        });

        mTextView = (TextView) findViewById(R.id.textView1);
        String msg1 = "<font color='#000000'><b>Almost there! </b></font>";
        String msg2 = "<font color='#000000'>Please confirm your Service provider & region. </font>";

        mTextView.setText(Html.fromHtml(msg1 + msg2));

        gallery = (Gallery) findViewById(R.id.gallery);
        galleryImageAdapter= new ImageAdapter(this);
        gallery.setAdapter(galleryImageAdapter);
        gallery.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                Toast.makeText(getBaseContext(), "pic" + (position + 1) + " selected",
                        Toast.LENGTH_SHORT).show();
            }
        });

        mEditText = (EditText) findViewById(R.id.edit_region);
        mEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ProfileActivity.this, RegionActivity.class);
                startActivity(i);
                finish();
            }
        });

        mSaveButton = (Button) findViewById(R.id.save_button);
        mSaveButton.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
                finish();
            }
        });

        region = sharedPreference.getValue(context);
        mEditText.setText(region);
        mEditText.setSelection(mEditText.getText().length());

    }
}
