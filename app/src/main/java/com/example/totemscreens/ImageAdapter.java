package com.example.totemscreens;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import android.widget.ImageView;

/**
 * Created by user on 1/20/2016.
 */
public class ImageAdapter extends BaseAdapter {

    private Context context;
    public ImageAdapter(Context c)
    {
        context = c;
    }

    public int getCount() {
        return mImageIds.length;
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imageView = new ImageView(context);
        imageView.setImageResource(mImageIds[position]);
        imageView.setLayoutParams(new Gallery.LayoutParams(250, 230));
        imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
        return imageView;
    }

    public Integer[] mImageIds = {
            R.drawable.logo_sprite_aircel,
            R.drawable.logo_sprite_airtel,
            R.drawable.logo_sprite_bsnl,
            R.drawable.logo_sprite_docomo,
            R.drawable.logo_sprite_idea,
            R.drawable.logo_sprite_mtc,
            R.drawable.logo_sprite_reliance,
            R.drawable.logo_sprite_uninor

    };

}

