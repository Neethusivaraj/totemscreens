package com.example.totemscreens;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.Html;
import android.text.Selection;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by user on 1/18/2016.
 */
public class MobileActivity extends Activity {

    private Button mSubmitButton;
    private EditText mEditText;
    private ImageButton mImageButton;
    private View view;
    private Dialog mBottomSheetDialog;
    private TextView mOtpTextView;
    private TextView mRequestTextView;
    private TextView mManualTextView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_mobile);

        mImageButton = (ImageButton) findViewById(R.id.back_topbar);
        mImageButton.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
                Intent in = new Intent(MobileActivity.this, SignInActivity.class);
                startActivity(in);
                finish();
            }
        });

        mEditText = (EditText) findViewById(R.id.edit_number);

        mEditText.setText("  +91   ");
        Selection.setSelection(mEditText.getText(), mEditText.getText().length());
        mEditText.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!s.toString().contains("  +91   ")) {
                    mEditText.setText("  +91   ");
                    Selection.setSelection(mEditText.getText(), mEditText.getText().length());
                }
            }
        });

        mSubmitButton = (Button) findViewById(R.id.submit_button);

    }

    public static int WAIT_TIME = 3000;

    public void openBottomSheet(View v) {

        if (validate()) {
            view = getLayoutInflater().inflate(R.layout.activity_auth_bottomsheet, null);
            mManualTextView = (TextView) view.findViewById(R.id.txt_manual);
            bottomSheet();

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {

                    mBottomSheetDialog.dismiss();
                    view = getLayoutInflater().inflate(R.layout.activity_done_bottomsheet, null);
                    bottomSheet();

                    new Handler().postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            mBottomSheetDialog.dismiss();
                            view = getLayoutInflater().inflate(R.layout.activity_otp_bottomsheet, null);
                            bottomSheet();

                            mOtpTextView = (TextView) view.findViewById(R.id.textView_otp);
                            mOtpTextView.setOnClickListener(new View.OnClickListener() {

                                @Override
                                public void onClick(View v) {
                                    Toast.makeText(MobileActivity.this, "Entered OTP", Toast.LENGTH_SHORT).show();

                                    Intent in = new Intent(MobileActivity.this, ProfileActivity.class);
                                    startActivity(in);
                                    mBottomSheetDialog.dismiss();
                                    finish();
                                }
                            });

                            mRequestTextView = (TextView) view.findViewById(R.id.request_otp);
                            String msg1 = "Didn't recieve OTP? ";
                            String msg2 = "<font color='#1a8cff'>Request New + </font>";
                            mRequestTextView.setText(Html.fromHtml(msg1 + msg2));
                            mRequestTextView.setOnClickListener(new View.OnClickListener() {

                                @Override
                                public void onClick(View v) {
                                    Toast.makeText(MobileActivity.this, "Enter OTP Manually", Toast.LENGTH_SHORT).show();
                                    Intent in = new Intent(MobileActivity.this, OTPAlternateActivity.class);
                                    startActivity(in);
                                    mBottomSheetDialog.dismiss();
                                    finish();
                                }
                            });
                        }
                    }, WAIT_TIME);
                }
            }, WAIT_TIME);

            String msg1 = "Thanks. We have sent a One Time Password to your number. " +
                    "Please give us a moment to verify. Stuck on verification? ";
            String msg3 = "<font color='#1a8cff'>Enter OTP manually. </font>";
            mManualTextView.setText(Html.fromHtml(msg1 + msg3));
            mManualTextView.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    Toast.makeText(MobileActivity.this, "Enter OTP Manually", Toast.LENGTH_SHORT).show();
                    Intent in = new Intent(MobileActivity.this, OTPAlternateActivity.class);
                    startActivity(in);
                    mBottomSheetDialog.dismiss();
                    finish();
                }
            });

        } else {
            onAuthenticationFailed();
            return;
        }
    }

    public void onAuthenticationFailed() {
        Toast.makeText(getBaseContext(), "Authentication failed", Toast.LENGTH_LONG).show();

        mSubmitButton.setEnabled(true);
    }

    public boolean validate() {
        boolean valid = true;

        String number = mEditText.getText().toString();

        if (number.isEmpty() || number.length() < 18 || number.length() > 18) {
            mEditText.setError("Incorrect Number");
            valid = false;
        } else {
            mEditText.setError(null);
        }

        return valid;
    }

    public void bottomSheet() {
        mBottomSheetDialog = new Dialog(MobileActivity.this, R.style.MaterialDialogSheet);
        mBottomSheetDialog.setContentView(view);
        mBottomSheetDialog.setCancelable(true);
        mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        mBottomSheetDialog.getWindow().setGravity(Gravity.BOTTOM);
        mBottomSheetDialog.show();
    }
}