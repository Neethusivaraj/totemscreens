package com.example.totemscreens;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by user on 1/22/2016.
 */
public class SharedPreference {

        public static final String PREFS_NAME = "REGION_PREFS";
        public static final String PREFS_KEY = "REGION";

        public SharedPreference() {
            super();
        }

        public void save(Context context, String text) {
            SharedPreferences settings;
            SharedPreferences.Editor editor;

            settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
            editor = settings.edit();
            editor.putString(PREFS_KEY, text);
            editor.commit();
        }

        public String getValue(Context context) {
            SharedPreferences settings;
            String text;
            settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
            text = settings.getString(PREFS_KEY, null);
            return text;
        }

}
