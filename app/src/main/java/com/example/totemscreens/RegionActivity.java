package com.example.totemscreens;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by user on 1/21/2016.
 */
public class RegionActivity extends Activity implements AdapterView.OnItemClickListener {

    private ListView listView;
    private ImageButton mImageButton;
    private SharedPreference sharedPreference;
    Activity context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_region);
        sharedPreference = new SharedPreference();
        listView = (ListView) findViewById(R.id.regionList);
        listView.setOnItemClickListener(this);

        mImageButton = (ImageButton) findViewById(R.id.back_topbar);
        mImageButton.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
                Intent in = new Intent(RegionActivity.this, ProfileActivity.class);
                startActivity(in);
                finish();
            }
        });
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        Toast.makeText(getApplicationContext(), ((TextView) view).getText(),
                Toast.LENGTH_SHORT).show();
        String region = ((TextView) view).getText().toString();
        System.out.println("Region  :" +region);

        sharedPreference.save(context, region);
    }
}
