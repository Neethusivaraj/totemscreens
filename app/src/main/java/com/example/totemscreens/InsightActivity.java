package com.example.totemscreens;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;

/**
 * Created by user on 1/18/2016.
 */
public class InsightActivity extends Activity {
    private ImageView mImageView1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_insight);

        mImageView1 = (ImageView) findViewById(R.id.slide_next);
        mImageView1.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
                Intent in = new Intent(InsightActivity.this, RecommendedPlanActivity.class);
                startActivity(in);
                finish();
            }
        });
    }
}
