package com.example.totemscreens;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;

public class MainActivity extends Activity {

    private ImageView mImageView1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);

        mImageView1 = (ImageView) findViewById(R.id.slide_next);
        mImageView1.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
                Intent in = new Intent(MainActivity.this, InsightActivity.class);
                startActivity(in);
                finish();
            }
        });
    }
}
